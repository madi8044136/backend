import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, name: 'product1', price: 100 },
  { id: 2, name: 'product2', price: 200 },
  { id: 3, name: 'product3', price: 300 },
];

let lastProductId = 4;
@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct = new Product();
    newProduct.id = lastProductId++;
    newProduct.name = createProductDto.name;
    newProduct.price = createProductDto.price;
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((products) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProducts: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProducts;
    return updateProducts;
  }

  remove(id: number) {
    const index = products.findIndex((products) => {
      return products.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteProducts = products[index];
    products.splice(index, 1);
    return deleteProducts;
  }
  reset() {
    products = [
      { id: 1, name: 'product1', price: 100 },
      { id: 2, name: 'product2', price: 200 },
      { id: 3, name: 'product3', price: 300 },
    ];
    lastProductId = 4;
    return 'RESET';
  }
}
