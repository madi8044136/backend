import { IsNotEmpty, MinLength, IsPositive } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  id: number;

  @IsNotEmpty()
  @MinLength(4)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  price: number;
}
